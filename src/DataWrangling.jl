"""
`get_binet_wol` returns a bipartite network given the numeric index of a web in the `web_of_life()` array

caveat: it's extremely ineffienct as it loads the full metadata of webs first.
"""
get_binet_wol = (n::Int) -> simplify(convert(BipartiteNetwork, web_of_life(web_of_life()[n].ID)))

"""
`get_binet_wol_id` returns a bipartite network given the string id of a web in the `web_of_life`

caveat: it's extremely ineffienct as it loads the full metadata of webs first.
"""
get_binet_wol_id = (id::String) -> simplify(convert(BipartiteNetwork, web_of_life(id)))

"""
`get_sadj_net` returns a sparse adjacency matrix from a (binary) Ecological Network
"""
get_sadj_net = (Observed_Network) -> sparse(Int.(Observed_Network.A))

"""
    lower_resolution(Matr::M,Rows::I, Columns::I,Rtresolution::N, Ctresolution::N) where {M<:AbstractMatrix, I<:AbstractArray, N<:Int}
    
Given a matrix `Matr` the function `lower_resolution`
partitions the indeces (rows and columns) of `Matr` in a number of _chunks_
(defined by Rtresolution and Ctresolution), sums up the values
in the resulting matrix blocks, and returns a
smaller matrix which entries are the block sums.

It is possible to specify a different reading order for the
rows and columns of Matr (the default one being `1:I`, `1:J` where
I and J are the number of rows and columns). This can be useful
for optimization methods based on the reordering of the matrix
indeces.
"""
function lower_resolution(
    Matr::M,
    Rtresolution::N, Ctresolution::N;
    Rows::I = collect(1:size(Matr)[1]),
    Columns::I = collect(1:size(Matr)[2])
    ) where {M<:AbstractMatrix, I<:AbstractArray, N<:Int}
    
    I_chunks = parti_indexes(Rows,Rtresolution)
    J_chunks = parti_indexes(Columns,Ctresolution)
    Rresolution, Cresolution = length(I_chunks), length(J_chunks)
    
    Low_dim = Array{Int64}(undef,Rresolution,Cresolution)
    
    for iₗ in 1:Rresolution, jₗ in 1:Cresolution
    @inbounds  Low_dim[iₗ, jₗ] = sum(Matr[I_chunks[iₗ],J_chunks[jₗ]])
    end
    
    return (lowm = sparse(Low_dim), I_chunks = I_chunks, J_chunks = J_chunks)
end

parti_indexes(Indexes, Resolution) = collect(Iterators.partition(Indexes, cld(length(Indexes),Resolution)))

"""
    value_count(Score_matrix::T) where T <: AbstractSparseMatrix

countes the number of entries in a score matrix at the values score level
and returns it as a sorted dictionary.
"""
function value_count(Score_matrix::T) where T <: AbstractSparseMatrix
    sort(countmap(Score_matrix.nzval), rev = true)
end

"""
    centre_mass(A_vec::T) where T <: AbstractVector

computes the position of the centre of mass of a *binary* vector.
Tests for entries being either ones or zeros.
Uses optimized methods for dense and sparse vectors.
"""
function centre_mass(A_vec::T)::Float64 where T <: AbstractVector
    @assert is_binary(A_vec)
    return _centre_mass(A_vec)
end

function _centre_mass(A_vec::T)::Float64 where T <: DenseVector
    where_not_zero = collect(1:length(A_vec))[A_vec .> 0]
    return sum(where_not_zero)/length(where_not_zero)
end

function _centre_mass(A_vec::T)::Float64 where T <: SparseVector
    return sum(A_vec.nzind)/length(A_vec.nzind)
end

