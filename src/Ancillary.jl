"""
    swap!(Xx::A,to_swap)::A where {A<:AbstractArray}

swaps two (or more) indexes, defined in `to_swap`, reversing
their order in the abstract array `Xx`
"""
function swap!(Xx::A,to_swap)::A where {A<:AbstractArray}
    Xx[to_swap] = Xx[reverse(to_swap)]
end

"""
    shuffle(Matr::T) where {T <: AbstractMatrix}

shuffles the rows and columns (independently) in a matrix `Matr`.
It does not preserve symmetry.
Returns a permuted matrix.
"""
function shuffle(Matr::T) where {T <: AbstractMatrix}
    I, J = size(Matr)
    shuffle_rows = sample(1:I, I, replace = false)
    shuffle_cols = sample(1:J, J, replace = false)
    return permute(Matr,shuffle_rows,shuffle_cols)
end

"""
    shuffle(Matr::T) where {T <: AbstractMatrix}

shuffles the rows and columns (together) in a square matrix `Matr`.
It does preserve symmetry.
Returns a permuted matrix.
"""
function shuffle_sym(Matr::T) where {T <: AbstractMatrix}
    @assert size(Matr)[1] == size(Matr)[2]
    I = size(Matr)[1]
    shuffle_cols = shuffle_rows = sample(1:I, I, replace = false)
    return permute(Matr,shuffle_rows,shuffle_cols)
end


function is_binary(Vect)::Bool
    all(entry -> entry ∈ [zero(eltype(Vect)), one(eltype(Vect))], Vect)
end