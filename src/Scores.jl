"""
    diagonal_scores(I::T, J::T, α::F = 0.1, k::F = 0.2; f::D = fancy) where {T <: Int, F <: Number, D <: Function}

computes a matrix of potential diagonal scores for an
`I×J` matrix. The individual element scores are defined as
`aᵈ` where `α` is a user set parameters (lower α corresponds
to faster decay of the score when moving further from the diagonal)
and for an entry Aᵢⱼ `d = f(i,j)` and `f` is distance function (`fancy` by
default).
The score matrix is truncated so that values at a distance greater
than `k` are set to zero.
"""
function diagonal_scores(I::T, J::T,α::N = 0.1, k::N = 0.2; f::D = fancy) where {T <: Int, N <: Number, D <: Function}

    Mdists = Array{Float64}(undef,I,J)
    
    # compute distance at each entry of the matrix
    for i in 1:I, j in 1:J
    @inbounds Mdists[i,j] = f(i,j,I,J)
    end

    # make distance relative to maximum distance
    Mdists .*= maximum(Mdists)^(-1)

    # translate from distance to score
    Mdists .= α.^Mdists

    # suppress entries at relative distance greater than k
    Mdists[Mdists .< α^k] .= 0

    # return matrix as sparse (works for small k)
    return sparse(Mdists)
end

"""
    normalize_score(N_links::Int, Score_counts::D)::Float64 where D <: AbstractDict

computes the maximum realizable diagonal score for a
network with `N_links` number of links, and a potential
score count `Score_counts`.

The `Score_counts` can be obtained by first producing an
adequately sized `diagonal_scores` matrix and then counting
its scores with `count_value`.
"""
function normalize_score(N_links::Int, Score_counts::D)::Float64 where D <: AbstractDict

    Tot_links = N_links
    nz_scores, nz_seats = Score_counts.keys, Score_counts.vals
     
    d = 1
    score = 0
    while (Tot_links >= nz_seats[d]) && d < length(nz_seats)
        Tot_links -= nz_seats[d]
        score += nz_seats[d] *  nz_scores[d]
        d += 1
    end
    if d ≤ length(nz_seats)
        score += nz_scores[d] *  minimum([Tot_links, nz_seats[d]])
    end
    return score
end

"""
    raw_diagonality(Matr, Matrix_of_Scores)::Float64

computes the *raw* diagonal score of an observed matrix by multiplying
it with a matrix of scores (which you can get using `diagonal_scores`).
"""
raw_diagonality(Matr, Matrix_of_Scores)::Float64 = sum(Matr .* Matrix_of_Scores)

"""
    raw_diagonality(Matr, Matrix_of_Scores, Norm_factor)::Float64

computes a normalized diagonal score of an observed matrix by multiplying
it with a matrix of scores (which you can get using `diagonal_scores`)
and a given normalization.
"""
raw_diagonality(Matr, Matrix_of_Scores, Norm_factor)::Float64 = raw_diagonality(Matr, Matrix_of_Scores) / Norm_factor

"""
    lowr_sa_optimization_diag(M::T, Matrix_of_Scores::T; λ = 0.999, t0 = 2.0, target = 1.1, power_of = 2.2, Nsteps = 10000) where {T <: AbstractMatrix}

performs simulated annealing optimization of an observed
network adjacency matrix `M` to recover the highest possible
raw diagonaliy computed against a matrix of scores `Matrix_of_scores`
(or a normalized diagonality if a normalization score `Norm_factor` is provided).

returns the indexes of rows and columns for the best matrix found
"""
function lowr_sa_optimization_diag(M,
    Matrix_of_Scores;
    Rs = collect(1:size(M)[1]),
    Cs = collect(1:size(M)[2]),
    λ = 0.999, t0 = 2.0,
    target = 1.1, power_of = 2,
    Nsteps = sum(size(M)) * 1_000)
        
    diag_here(Matr) = raw_diagonality(Matr, Matrix_of_Scores)
    
    Δbest = (diag_here(permute(M,Rs,Cs))-target)^power_of
    
    I, J  = size(M)
    
    for step in 1:Nsteps
        t = λ^(step-1)*t0
        # we work on either rows or columns at random
        if rand() < I/(I+J) # work on rows
            to_swap = sample(1:I,2,replace = false)
            swap!(Rs,to_swap)
            Δstate = (diag_here(permute(M,Rs,Cs))-target)^power_of
            Δ = Δstate - Δbest
            P = exp(-Δ/t)
            if (rand() ≤ P)
                Δbest = Δstate
            else
                swap!(Rs,to_swap)
            end
        else
            to_swap = sample(1:J,2,replace = false)
            swap!(Cs,to_swap)
            Δstate = (diag_here(permute(M,Rs,Cs))-target)^power_of
            Δ = Δstate - Δbest
            P = exp(-Δ/t)
            if (rand() ≤ P)
                Δbest = Δstate
            else
                swap!(Cs,to_swap)
            end
        end
    end
    return Rs, Cs
end

function sa_optimization_diag(M::T,
    Matrix_of_Scores::Ms,
    Norm_factor::N;
    Rows = collect(1:size(M)[1]),
    Columns = collect(1:size(M)[2]), 
    λ = 0.75, t0 = 1.3,
    target = 3.1, power_of = 2.5,
    Nsteps = sum(size(M)) * 1_000) where {T <: AbstractMatrix, Ms <: AbstractMatrix, N <: Number}
    
    diag_here(Matr) = raw_diagonality(Matr, Matrix_of_Scores, Norm_factor)

    Δbest = (target - diag_here(permute(M,Rows,Columns)))^power_of

    I, J  = size(M)
    
    for step in 1:Nsteps
        t = λ^(step-1)*t0
        # we work on either rows or columns at random
        if rand() < I/(I+J) # work on rows
            to_swap = sample(1:I,2,replace = false)
            swap!(Rows,to_swap)
            now_diag = diag_here(permute(M,Rows,Columns))
            Δstate = (target - now_diag)^power_of
            Δ = Δstate - Δbest
            P = exp(-Δ/t)
            if (rand() ≤ P)
                Δbest = Δstate
            else
                swap!(Rows,to_swap)
            end
        else
            to_swap = sample(1:J,2,replace = false)
            swap!(Columns,to_swap)
            now_diag = diag_here(permute(M,Rows,Columns))
            Δstate = (target - now_diag)^power_of
            Δ = Δstate - Δbest
            P = exp(-Δ/t)
            if (rand() ≤ P)
                Δbest = Δstate
            else
                swap!(Columns,to_swap)
            end
        end
    end
    return Rows, Columns
end

"""
    cm_optimization_diag!(Matr::T, steps::Int) where T <: AbstractMatrix

iteratively sort the rows and the columns of an observed
network adjacency matrix `Matr` computing their centre of mass with `centre_mass`.

permutes the input matrix in place
"""
function cm_optimization_diag!(Matr::T, steps::Int) where {T <: AbstractArray}

    moves = Array{Int}(undef,steps,2)


    R, C = size(Matr)

    Rs, Cs = collect(1:R), collect(1:C)

    row_rank = Array{Float64}(undef, R)
    col_rank = Array{Float64}(undef, C)

    for x in 1:steps
        for row in Rs
            @inbounds row_rank[row] = centre_mass(Matr[row,:])
        end
        
        new_row_order = sortperm(row_rank)
        @inbounds moves[x,1] = sum(new_row_order .!= Rs)
        @inbounds Matr .= permute(Matr, new_row_order, Cs)
        
        for col in Cs
            @inbounds  col_rank[col] = centre_mass(Matr[:,col])
        end
        
        new_col_order = sortperm(col_rank)
        @inbounds moves[x,2] = sum(new_col_order .!= Cs)
        @inbounds Matr .= permute(Matr, Rs, new_col_order)

        moves[x,:] == [0,0] && break

    end

end

"""
    cm_optimization_diag(Matr::T, steps::Int) where T <: AbstractMatrix

iteratively sort the rows and the columns of an observed
network adjacency matrix `Matr` computing their centre of mass with `centre_mass`.

returns the new sorted indexes
"""
function cm_optimization_diag(Matr::T, steps::Int;
    Rs = collect(1:size(Matr)[1]),
    Cs = collect(1:size(Matr)[2])) where {T <: AbstractSparseMatrix}

    Do_nothing_sort_rows = collect(1:size(Matr)[1])
    Do_nothing_sort_columns = collect(1:size(Matr)[2])

    new_row_order,new_col_order  = similar(Rs), similar(Cs)
    for x in 1:steps

        @inbounds new_row_order .= sortperm(mapslices(centre_mass,permute(Matr,Rs,Cs),dims = 2).nzval)
        moves_1 = sum(new_row_order .!= Do_nothing_sort_rows)
        permute!(Rs,new_row_order)
        
        @inbounds new_col_order .= sortperm(mapslices(centre_mass,permute(Matr,Rs,Cs),dims = 1).nzval)
        moves_2 = sum(new_col_order .!= Do_nothing_sort_columns)
        permute!(Cs,new_col_order)

        (moves_1,moves_2) == (0,0) && break

    end

    return Rs, Cs

end