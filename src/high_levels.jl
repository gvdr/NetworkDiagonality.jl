"""
    diagonality(Network)::Float64

computes a normalized diagonal score of an observed matrix by multiplying
it with a matrix of scores (which you can get using `diagonal_scores`)
and a given normalization.
"""
function diagonality(Network::T) where {T <: AbstractBipartiteNetwork} 
    diagonality_bipartite(Network.A)
end

function diagonality(Network::T) where {T <: AbstractUnipartiteNetwork}
    diagonality_unipartite(Network.A)
end

"""
    diagonality_bipartite(Matrix; kwargs)::Float64

computes a normalized diagonal score of an observed matrix by multiplying
it with a matrix of scores (which you can get using `diagonal_scores`)
and a given normalization.
"""
function diagonality_bipartite(Matrix::Sm;
    α::N = 0.1, k::N = 0.2, f = fancy,
    λ = 0.999, t0 = 2.0,
    target = 1.1, power_of = 2,
    low_res_steps = 8) where {Sm <: SparseMatrixCSC, N <: Number}

    sizeM = size(Matrix)
    
    rxs = collect(1:sizeM[1])
    cxs = collect(1:sizeM[2])

    Ltot = sum(Matrix)
    # plan:
    # we first centre mass sort rows and columns
    # than go with low resolution approximations
    # and finally simulated annealing

    Scoreₕᵢ =  diagonal_scores(sizeM[1], sizeM[2], α, k, f = f)
    NormFactorₕᵢ = normalize_score(Ltot,value_count(Scoreₕᵢ))

    diag_score = raw_diagonality(permute(Matrix,rxs,cxs), Scoreₕᵢ, NormFactorₕᵢ)
    
    # low resolution annealing

    lowr_size_target, lowc_size_target  = sizeM .|> sqrt .|> ceil
    row_steps = Int.(ceil.(range(2, stop=lowr_size_target, length=low_res_steps)))
    col_steps = Int.(ceil.(range(2, stop=lowc_size_target, length=low_res_steps)))


    for step in 1:low_res_steps
        r_step, c_step = row_steps[step], col_steps[step]
        Scoreₗₒ =  diagonal_scores(r_step,c_step,α, k, f = f)

        Matrₗₒ, rchunks, cchunks = lower_resolution(Matrix,r_step,c_step, Rows = rxs, Columns = cxs)
        rₗₒs, cₗₒs = lowr_sa_optimization_diag(Matrₗₒ, Scoreₗₒ, target = Ltot)

        rxs = vcat(rchunks[rₗₒs]...)
        cxs = vcat(cchunks[cₗₒs]...)
        
        diag_score = raw_diagonality(permute(Matrix,rxs,cxs), Scoreₕᵢ, NormFactorₕᵢ)
    end



    # centre mass initialization

    rxs, cxs = cm_optimization_diag(Matrix, maximum(sizeM), Rs = rxs, Cs = cxs)

    diag_score = raw_diagonality(permute(Matrix,rxs,cxs), Scoreₕᵢ, NormFactorₕᵢ)

    # high resolution annealing

    rxs, cxs = sa_optimization_diag(Matrix,
        Scoreₕᵢ,
        NormFactorₕᵢ,
        Rows = rxs,
        Columns = cxs)

    diag_score = raw_diagonality(permute(Matrix,rxs,cxs), Scoreₕᵢ, NormFactorₕᵢ)

    return (diagonality_score = diag_score, Rs = rxs, Cs = cxs)
end

function diagonality_bipartite(Matrix::Dm) where {Dm <: Matrix}
    diagonality_bipartite(sparse(Matrix))
end

"""
    diagonality_unipartite(Matrix)::Float64

computes a normalized diagonal score of an observed matrix by multiplying
it with a matrix of scores (which you can get using `diagonal_scores`)
and a given normalization.
"""
function diagonality_unipartite(Matrix::Sm;
    λ = 0.999, t0 = 2.0,
    target = 1.1, power_of = 2) where Sm <: SparseMatrixCSC

nothing
   # return diag_score
end

function diagonality_unipartite(Matrix::Dm) where {Dm <: Matrix}
    diagonality_unipartite(sparse(Matrix))
end