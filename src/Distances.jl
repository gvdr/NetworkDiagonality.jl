"""
`fancy` computes the distance of a matrix entry from the matrix diagonal using a
geometric mean normalization.
"""
fancy = (x,y,I,J) -> abs.(round.(((J.*x).-(I.*y))/sqrt(I*J)))