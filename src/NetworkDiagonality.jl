module NetworkDiagonality

using EcologicalNetworks
using LinearAlgebra
using Random
using SparseArrays
using StatsBase

include(joinpath(".", "Ancillary.jl"))
export swap!,
    shuffle

include(joinpath(".", "DataWrangling.jl"))
export get_binet_wol,
    get_binet_wol_id,
    get_sadj_net,
    lower_resolution,
    value_count,
    centre_mass

include(joinpath(".", "Distances.jl"))
export fancy

include(joinpath(".", "Scores.jl"))
export raw_diagonality,
    diagonal_scores,
    normalize_score,
    lowr_sa_optimization_diag,
    sa_optimization_diag,
    cm_optimization_diag,
    cm_optimization_diag!

include(joinpath(".", "high_levels.jl"))
export diagonality_bipartite

end
