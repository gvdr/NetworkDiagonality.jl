# NetworkDiagonality

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://gvdr.gitlab.io/NetworkDiagonality.jl/dev)
[![Build Status](https://gitlab.com/gvdr/NetworkDiagonality.jl/badges/master/pipeline.svg)](https://gitlab.com/gvdr/NetworkDiagonality.jl/pipelines)
[![Coverage](https://gitlab.com/gvdr/NetworkDiagonality.jl/badges/master/coverage.svg)](https://gitlab.com/gvdr/NetworkDiagonality.jl/commits/master)
[![Code Style: Blue](https://img.shields.io/badge/code%20style-blue-4495d1.svg)](https://github.com/invenia/BlueStyle)
[![ColPrac: Contributor's Guide on Collaborative Practices for Community Packages](https://img.shields.io/badge/ColPrac-Contributor's%20Guide-blueviolet)](https://github.com/SciML/ColPrac)


A small and work in progress package to compute the diagonality score of a network.

It does it by deployng a bunch of optimization techniques, which can probably be improved.