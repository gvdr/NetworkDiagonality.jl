using NetworkDiagonality
using Documenter

makedocs(;
    modules=[NetworkDiagonality],
    authors="Giulio Valentino Dalla Riva <me@gvdallariva.net> and contributors",
    repo="https://gitlab.com/gvdr/NetworkDiagonality.jl/blob/{commit}{path}#L{line}",
    sitename="NetworkDiagonality.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://gvdr.gitlab.io/NetworkDiagonality.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
